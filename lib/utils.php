<?php

function lg( $message )
{
  echo $message . "\n";
}

function test( $name, $success )
{
  if ( !$success )
  {
    lg( $name . ": NOK" );
    
    exit();
    
  }

  lg( $name . ": ok" );
}

function start( $file )
{
  lg( '******************' . basename( $file, '.php' ) . '******************' );
}

function done()
{
  lg( "done.\n" );
}