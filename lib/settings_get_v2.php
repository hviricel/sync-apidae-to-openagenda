<?php

if ( !isset( $GLOBALS[ 'OA_API_ENV' ] ) )
{
  $GLOBALS[ 'OA_API_ENV' ] = 'production';
}


function settings_get_v2( $key, $agendaUid )
{
  $route = $GLOBALS[ 'OA_API_ENV' ] !== 'development' ? 
    "https://api.openagenda.com/v2/agendas/$agendaUid/settings?key=$key" :
    "https://dapi.openagenda.com/v2/agendas/$agendaUid/settings?key=$key";

  $ch = curl_init();

  if ( $GLOBALS[ 'OA_API_ENV' ] === 'development' )
  {
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
  }

  curl_setopt( $ch, CURLOPT_URL, $route );
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

  $received_content = curl_exec($ch);

  return json_decode( $received_content, true );

}