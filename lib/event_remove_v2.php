<?php

if ( !isset( $GLOBALS[ 'OA_API_ENV' ] ) )
{
  $GLOBALS[ 'OA_API_ENV' ] = 'production';
}

function event_remove_v2( $accessToken, $agendaUid, $eventUid, $options = array() )
{
  extract( array_merge( array(
  ), $options ) );

  $imageLocalPath = null;

  if ( isset( $data[ 'image' ] ) )
  {
    $imageLocalPath = $data[ 'image' ];

    unset( $data[ 'image' ] );
  }

  $route = $GLOBALS[ 'OA_API_ENV' ] !== 'development' ? 
    "https://api.openagenda.com/v2/agendas/$agendaUid/events/$eventUid" :
    "https://dapi.openagenda.com/v2/agendas/$agendaUid/events/$eventUid";

  $ch = curl_init();

  if ( $GLOBALS[ 'OA_API_ENV' ] === 'development' )
  {
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
  }

  curl_setopt( $ch, CURLOPT_URL, $route );
  curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

  curl_setopt($ch, CURLOPT_POSTFIELDS, array(
   'access_token' => $accessToken,
   'nonce' => rand()
  ) );

  $received_content = curl_exec($ch);

  return json_decode( $received_content, true );
}