<?php

if ( !isset( $GLOBALS[ 'OA_API_ENV' ] ) )
{
  $GLOBALS[ 'OA_API_ENV' ] = 'production';
}

function location_create( $accessToken, $data )
{
  $route = $GLOBALS[ 'OA_API_ENV' ] !== 'development' ? 'https://api.openagenda.com/v1/locations' : 'https://dapi.openagenda.com/frontend_dev.php/v1/locations';

  $ch = curl_init();

  if ( $GLOBALS[ 'OA_API_ENV' ] === 'development' )
  {
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
  }

  curl_setopt( $ch, CURLOPT_URL, $route );
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
  curl_setopt($ch, CURLOPT_POST, true);

  curl_setopt($ch, CURLOPT_POSTFIELDS, array(
   'access_token' => $accessToken,
   'nonce' => rand(),
   'data' => json_encode( $data )
  ) );

	/*echo '<pre>';
	var_dump( $data );
	echo '</pre>';*/

  $received_content = curl_exec($ch);

  //var_dump( $received_content );

  return json_decode( $received_content, true )[ 'uid' ];
}
