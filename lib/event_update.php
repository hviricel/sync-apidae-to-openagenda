<?php

if ( !isset( $GLOBALS[ 'OA_API_ENV' ] ) )
{
  $GLOBALS[ 'OA_API_ENV' ] = 'production';
}

function event_update( $accessToken, $eventUid, $data, $options = array() )
{
  extract( array_merge( array(
    'lang' => null
  ), $options ) );

  $imageLocalPath = null;

  if ( isset( $data[ 'image' ] ) )
  {
    $imageLocalPath = $data[ 'image' ];

    unset( $data[ 'image' ] );
  }

  $route = $GLOBALS[ 'OA_API_ENV' ] !== 'development' ? 
    "https://api.openagenda.com/v1/events/$eventUid":
    "https://dapi.openagenda.com/frontend_dev.php/v1/events/$eventUid";

  $ch = curl_init();

  if ( $GLOBALS[ 'OA_API_ENV' ] === 'development' )
  {
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
  }

  curl_setopt( $ch, CURLOPT_URL, $route );
  curl_setopt( $ch, CURLOPT_RETURNTRANSFER, TRUE);
  curl_setopt( $ch, CURLOPT_POST, true);

  $posted = array(
   'access_token' => $accessToken,
   'nonce' => rand(),
   'data' => json_encode( $data ),
   'lang' => $lang
  );

  if ( $imageLocalPath )
  {
    $posted[ 'image' ] = $imageLocalPath;
  }

  curl_setopt($ch, CURLOPT_POSTFIELDS, $posted );

  $received_content = curl_exec($ch);

  var_dump( $received_content );

  return intval( json_decode( $received_content, true )[ 'uid' ] );
}