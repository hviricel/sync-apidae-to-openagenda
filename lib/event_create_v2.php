<?php

if ( !isset( $GLOBALS[ 'OA_API_ENV' ] ) )
{
  $GLOBALS[ 'OA_API_ENV' ] = 'production';
}

function event_create_v2( $accessToken, $agendaUid, $data, $options = array() )
{
  extract( array_merge( array(
  ), $options ) );

  $imageLocalPath = null;

  if ( isset( $data[ 'image' ] ) && isset( $data[ 'image' ][ 'file' ] ) )
  {
    $imageLocalPath = $data[ 'image' ][ 'file' ];

    unset( $data[ 'image' ][ 'file' ] );
  }

  $route = $GLOBALS[ 'OA_API_ENV' ] !== 'development' ?
    "https://api.openagenda.com/v2/agendas/$agendaUid/events" :
    "https://dapi.openagenda.com/v2/agendas/$agendaUid/events";

  $ch = curl_init();

  if ( $GLOBALS[ 'OA_API_ENV' ] === 'development' )
  {
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
  }

  curl_setopt( $ch, CURLOPT_URL, $route );
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
  curl_setopt($ch, CURLOPT_POST, true);

  $posted = array(
   'access_token' => $accessToken,
   'nonce' => rand(),
   'data' => json_encode( $data )
  );

  if ( $imageLocalPath )
  {
    $posted[ 'image' ] = $imageLocalPath;
  }

  curl_setopt($ch, CURLOPT_POSTFIELDS, $posted );

  $received_content = curl_exec($ch);

  return json_decode( $received_content, true );
}
