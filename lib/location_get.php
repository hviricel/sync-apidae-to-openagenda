<?php

if ( !isset( $GLOBALS[ 'OA_API_ENV' ] ) )
{
  $GLOBALS[ 'OA_API_ENV' ] = 'production';
}

function location_get( $key, $locationUid )
{
  $route = $GLOBALS[ 'OA_API_ENV' ] !== 'development' ? 
    "https://api.openagenda.com/v1/locations/$locationUid?key=$key" : 
    "https://dapi.openagenda.com/frontend_dev.php/v1/locations/$locationUid?key=$key";

  $ch = curl_init();

  if ( $GLOBALS[ 'OA_API_ENV' ] === 'development' )
  {
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
  }

  curl_setopt( $ch, CURLOPT_URL, $route );
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
  
  $received_content = curl_exec($ch);

  return json_decode( $received_content, true )[ 'data' ];
}