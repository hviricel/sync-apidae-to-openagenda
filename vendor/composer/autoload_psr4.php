<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Symfony\\Component\\Finder\\' => array($vendorDir . '/symfony/finder'),
    'Sitra\\Tests\\' => array($vendorDir . '/sitra-tourisme/sitra-api-php/tests'),
    'Sitra\\ApiClient\\' => array($vendorDir . '/sitra-tourisme/sitra-api-php/src/Sitra/ApiClient'),
    'Psr\\Http\\Message\\' => array($vendorDir . '/psr/http-message/src'),
    'Mmoreram\\Extractor\\' => array($vendorDir . '/mmoreram/extractor/src/Extractor'),
    'GuzzleHttp\\Psr7\\' => array($vendorDir . '/guzzlehttp/psr7/src'),
    'GuzzleHttp\\Promise\\' => array($vendorDir . '/guzzlehttp/promises/src'),
    'GuzzleHttp\\Command\\Guzzle\\' => array($vendorDir . '/guzzlehttp/guzzle-services/src'),
    'GuzzleHttp\\Command\\' => array($vendorDir . '/guzzlehttp/command/src'),
    'GuzzleHttp\\' => array($vendorDir . '/guzzlehttp/guzzle/src'),
);
