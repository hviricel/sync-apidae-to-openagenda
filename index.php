<?php

include 'config.php';
include 'lib/utils.php';
include 'lib/location_create.php';
include 'lib/location_get.php';
include 'lib/access_token_get.php';
include 'lib/event_create_v2.php';
include 'lib/event_update_v2.php';
include 'lib/event_get.php';
include 'lib/agenda_event_add.php';

$accessToken = access_token_get($keys['secret'] );

/*
 *
 *  Variables Apidae
 *
 */




$requete = array();

/*
 *
 *  Requêtes Apidae
 *
 */

$requete['apiKey'] = $apiKey;
$requete['projetId'] = $projetId;
$requete['count'] = $nbResult;
$requete['territoireIds'] = $territoireIds;
$requete['dateDebut'] = date("Y-m-d");
//$requete['dateDebut'] = "2020-01-21";
$requete["responseFields"] = array("@all");
$url = $apiDomain."v002/agenda/detaille/list-objets-touristiques/";
$url .= '?';
$url .= 'query='.urlencode(json_encode($requete));


/*
 *
 *  Retour événements Apidae
 *
 */


$resultsJSON = callAPI('POST', $url);
$results = json_decode($resultsJSON);


/*
 *
 *  Adaptation des données Apidae au format OpenAgenda
 *
 */

$ret = $results->objetsTouristiques;

foreach ($ret as $key => $jour) {

    foreach ($jour as $result) {

        $loc = $result->localisation;
        $open = $result->ouverture;
        $timings = array();

        if (isset($result->nom->libelleFr) && !empty($result->nom->libelleFr)) {
            $titre = $result->nom->libelleFr;
        } else {
            continue;
        }
        if (isset($result->presentation->descriptifCourt->libelleFr) && !empty($result->presentation->descriptifCourt->libelleFr)) {
            $desc = $result->presentation->descriptifCourt->libelleFr;
        } else {
            $desc = $result->nom->libelleFr;
        }
        if (isset($result->presentation->descriptifDetaille->libelleFr)) {
            $desc_long = $result->presentation->descriptifDetaille->libelleFr;
        } else {
            $desc_long = "";
        }


        if (isset($open->ouvertTouteLAnnee)) {
            continue;
        }

        if (isset($open->periodesOuvertures) && count($open->periodesOuvertures) > 1) {
            continue;
        }

        if (!isset($loc->geolocalisation->geoJson->coordinates['1']) || !isset($loc->geolocalisation->geoJson->coordinates['0']) || empty($loc->geolocalisation->geoJson->coordinates['1']) || empty($loc->geolocalisation->geoJson->coordinates['0'])) {
            continue;
        }

        $datetime1 = date_create($open->periodesOuvertures[0]->dateDebut);
        $datetime2 = date_create($open->periodesOuvertures[0]->dateFin);
        $interval = date_diff($datetime1, $datetime2);
        $exist = date_diff($datetime1, date_create($key));

        if ($interval->format('%a') < $dureemax && $exist->format('%a') < 1) {

            if ( isset( $open->periodesOuvertures[0]->horaireOuverture ) ) {
                $openHour = $open->periodesOuvertures[0]->horaireOuverture;
            } else {
                $openHour = "00:00:00";
            }

            if ( isset( $open->periodesOuvertures[0]->horaireFermeture ) ) {
                $closeHour = $open->periodesOuvertures[0]->horaireFermeture;
            } else {
                $closeHour = "23:59:59";
            }

            for ( $i = $datetime1; $i <= $datetime2; $i->modify( '+1 day' ) ) {

                $datestr = $i->format('Y-m-d');

                $start = strtotime( $datestr . " " . $openHour);
                $end = strtotime( $datestr . " " . $closeHour);

                $begin = date( 'Y-m-d\TH:i:sO', $start );
                $end = date( 'Y-m-d\TH:i:sO', $end );

                $timings[] = array( 'begin' => $begin, 'end' => $end );
            }
        } else {
            continue;
        }

        $adresse = "";

        if (isset($loc->adresse->adresse1)) {
            $adresse .= $loc->adresse->adresse1 . " ";
        }
        if (isset($loc->adresse->adresse2)) {
            $adresse .= $loc->adresse->adresse2 . " ";
        }
        if (isset($loc->adresse->codePostal)) {
            $adresse .= $loc->adresse->codePostal . " ";
        }
        if (isset($loc->adresse->commune->nom)) {
            $adresse .= $loc->adresse->commune->nom . " ";
        }
        if (isset($loc->adresse->commune->pays->libelleFr)) {
            $adresse .= $loc->adresse->commune->pays->libelleFr;
        }

        if (isset($loc->adresse->nomDuLieu)) {
            $placename = $loc->adresse->nomDuLieu;
        } elseif (isset($loc->adresse->adresse1)) {
            $placename = $loc->adresse->adresse1;
        } elseif (isset($loc->adresse->commune->nom)) {
            $placename = $loc->adresse->commune->nom;
        } elseif (isset($loc->adresse->codePostal)) {
            $placename = $loc->adresse->codePostal;
        } else {
            continue;
        }

        $lieu = array(
            'placename' => $placename,
            'address' => $adresse,
            'latitude' => $loc->geolocalisation->geoJson->coordinates['1'],
            'longitude' => $loc->geolocalisation->geoJson->coordinates['0']
        );

        $idLieu = location_create($accessToken, $lieu);

        //location_get($keys["public"],$createLieu);

        $image['file'] = $result->illustrations[0]->traductionFichiers[0]->url;
        $image['url'] = $result->illustrations[0]->traductionFichiers[0]->url;
        $image['title'] = $result->illustrations[0]->traductionFichiers[0]->fileName;

        if (isset($result->descriptionTarif->tarifsEnClair)) {
            $conditions = $result->descriptionTarif->tarifsEnClair;
        } else {
            $conditions = "";
        }

        if (isset($result->prestations->ageMin)) {
            $age['min'] = $result->prestations->ageMin;
        } else {
            $age['min'] = "";
        }

        if (isset($result->prestations->ageMax)) {
            $age['max'] = $result->prestations->ageMax;
        } else {
            $age['max'] = "";
        }

        /*
         *
         *      Préparation des données à envoyer à OpenAgenda via API
         *
         */

        $eventData = array(
            'state' => 0,
            'title' => array('fr' => $titre),
            'description' => array('fr' => substr($desc, 0, 195) . '...'),
            'longDescription' => array('fr' => $desc . $desc_long),
            'locationUid' => $idLieu,
            'timings' => $timings,
            //'registration' =>
            'age' => $age,
            //'accessibility' =>
            'conditions' => $conditions,
            'image' => $image
        );


        $create = event_create_v2($accessToken, $agendaUid, $eventData);

    }

}



function callAPI($method, $url, $data = ""){
	$curl = curl_init();

	switch ($method){
		case "POST":
			curl_setopt($curl, CURLOPT_POST, 1);
			if ($data)
				curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
			break;
		case "PUT":
			curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
			if ($data)
				curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
			break;
		default:
			if ($data)
				$url = sprintf("%s?%s", $url, http_build_query($data));
	}

	// OPTIONS:
	curl_setopt($curl, CURLOPT_URL, $url);
	/*curl_setopt($curl, CURLOPT_HTTPHEADER, array(
		'Content-Type: application/json',
	));*/
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

	// EXECUTE:
	$result = curl_exec($curl);
	if(!$result){die("Connection Failure");}
	curl_close($curl);
	return $result;
}

